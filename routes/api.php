<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

$router->group(['prefix' => '/v1/advert','middleware' => 'checkSession'], function ($router) {
    // CreateUpdateDelete  объявления...
    $router->post('/', 'Api\AdvertController@createAdvert');
    $router->put('/{id}', 'Api\AdvertController@editAdvert');
    $router->delete('/{id}', 'Api\AdvertController@delAdvert');
});

$router->group(['prefix' => '/v1/advert/maincategory','middleware' => 'checkSession'], function ($router) {
    // CreateUpdateDelete  главные категории...
    $router->post('/', 'Api\CategoryController@createMainCategory');
    $router->put('/{id}', 'Api\CategoryController@editMainCategory');
    $router->delete('/{id}', 'Api\CategoryController@delMainCategory');
});

$router->group(['prefix' => '/v1/advert/subcategory','middleware' => 'checkSession'], function ($router) {
    // CreateUpdateDelete  подкатегории...
    $router->post('/', 'Api\CategoryController@createSubCategory');
    $router->put('/{id}', 'Api\CategoryController@editSubCategory');
    $router->delete('/{id}', 'Api\CategoryController@delSubCategory');
});

$router->group(['prefix' => '/v1/advert/favorite','middleware' => 'checkSession'], function ($router) {
    // CreateDeleteRead  favorites...
    $router->post('/', 'Api\FavoriteController@createFavorite');
    $router->delete('/{id}', 'Api\FavoriteController@delFavorite');
    $router->get('/', 'Api\FavoriteController@getFavorite');
});

$router->group(['prefix' => '/v1/advert/comment','middleware' => 'checkSession'], function ($router) {
    // CreateDeleteRead  favorites...
    $router->post('/', 'Api\CommentController@createComment');
    $router->delete('/{id}', 'Api\CommentController@delComment');
    $router->get('/', 'Api\CommentController@getComment');
});

$router->group(['prefix' => '/v1/advert/metric','middleware' => 'checkSession'], function ($router) {
    // Read  metrics...
    $router->get('/', 'Api\MetricController@getMetric');
    $router->get('/{id}', 'Api\MetricController@getMetricById');
});

$router->group(['prefix' => '/v1/advert'], function ($router) {
    // Read  maincategories...
    $router->get('/maincategory/', 'Api\CategoryController@getMainCategory');
    $router->get('/maincategory/{id}', 'Api\CategoryController@getMainCategoryById');
    // Read  subcategories...
    $router->get('/subcategory/', 'Api\CategoryController@getSubCategory');
    $router->get('/subcategory/{id}', 'Api\CategoryController@getSubCategoryById');

    // Read  объявления...
    $router->get('/', 'Api\AdvertController@getAdvert');
    $router->get('/{id}', 'Api\AdvertController@getAdvertById');
});
