<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class CheckSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       // return $next($request);
        $session = $request->header('SESSION-ID');
        if (!$session) {
            return (new Response([
                'code' => HTTP_BAD_REQUEST,
                'message' => 'Token Required'
            ], HTTP_BAD_REQUEST, ['description' => 'Token Required']));
        }
        $headers = ['SESSION-ID' => $session];
        $client = new \GuzzleHttp\Client([
            'headers' => $headers,
            'exceptions' => false
        ]);

        $response = $client->request('POST', 'http://localhost:8000/api/v1/session/check', []);
        if ($response->getStatusCode() != 200)
        {
            $response = $response->getBody()->getContents();
            return (new Response([
                'code' => HTTP_FORBIDDEN,
                'message' => $response
            ], HTTP_FORBIDDEN, ['description' => 'Check session error']));

        }
        else return $next($request);
    }
}
