<?php

namespace App\Http\Controllers\Api;

use App\Models\Metric;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\DB;

class MetricController extends Controller
{
    //
    public function getMetric(Request $request){

        $query = $request->query();

        if (!isset($query['filter'])) $query['filter'] = '';
        if (!isset($query['orderByField'])) $query['orderByField'] = 'id';
        if (!isset($query['orderByAsc'])) $query['orderByAsc'] = 'asc';

        //check orderByField on exist field and orderByAsc on asc/desc

        $filter = $query['filter'];
        if (isset($query['advert_id'])) {
            $metrics = DB::table('metrics')
                ->where('advert_id', '=', $query['advert_id'])
                ->where(function($query1) use ($filter) {
                    $query1->where('ip_address', 'LIKE', '%' .$filter.'%');
                    $query1->orWhere('referer', 'LIKE', '%' .$filter.'%');
                    $query1->orWhere('user_agent', 'LIKE', '%' .$filter.'%');
                })
                ->orderBy($query['orderByField'], $query['orderByAsc'])
                ->get();
            return response(new Response($metrics, HTTP_OK, ['description' => 'Metrics get OK']))
                ->header('Content-Type', 'application/json');
        }
        else {
            $metrics = Metric::where('ip_address', 'LIKE', '%' . $query['filter'] . '%')
                ->orWhere('referer', 'LIKE', '%' . $query['filter'] . '%')
                ->orWhere('user_agent', 'LIKE', '%' . $query['filter'] . '%')
                ->orderBy($query['orderByField'], $query['orderByAsc'])
                ->get();

            return response(new Response($metrics, HTTP_OK, ['description' => 'Metrics get OK']))
                ->header('Content-Type', 'application/json');
        }
    }
    public function getMetricById(Request $request, $id){
        $requestParams = ['id' => $id];

        $validationRules = [
            'id' => 'required|integer|exists:metrics,id'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationRules
        );

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error),
                HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }

        $metric = DB::table('metrics')
            ->where('id','=',$id)
            ->get();

        return response(new Response($metric, HTTP_OK, ['description' => 'Metric get by id OK']))
            ->header('Content-Type', 'application/json');

    }
}
