<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\Comment;
use Illuminate\Support\Facades\DB;

class CommentController extends Controller
{
    //
    public function createComment(Request $request)
    {
        $requestParams = $request->only(
            'text',
            'user_id',
            'advert_id'
        );

        $validationRules = [
            'text' => 'required|min:3',
            'user_id' => 'required|integer|exists:users,id',
            'advert_id' => 'required|integer|exists:adverts,id'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationRules
        );

        $requestParams['status'] = 1;

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error),
                HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }

        $existComment = DB::table('comments')
            ->where('text','=',$requestParams['text'])
            ->where('user_id','=',$requestParams['user_id'])
            ->where('advert_id','=',$requestParams['advert_id'])
            ->get();
        if (count($existComment) > 0) {
            return (new Response([
                'code' => HTTP_FOUND,
                'message' => 'Comment with these params already exists'
            ], HTTP_FOUND, ['description' => 'Comment exists']));
        }

        $comment = new Comment($requestParams);
        $comment->save();

        return response(new Response([
            'id' => $comment->id,
            'text' => $requestParams['text'],
            'user_id' => $requestParams['user_id'],
            'advert_id' => $requestParams['advert_id'],
        ], HTTP_OK, ['description' => 'Comment create OK']));
    }

    public function getComment(Request $request){

        $query = $request->query();

        if (!isset($query['filter'])) $query['filter'] = '';
        if (!isset($query['orderByField'])) $query['orderByField'] = 'id';
        if (!isset($query['orderByAsc'])) $query['orderByAsc'] = 'asc';

        //check orderByField on exist field and orderByAsc on asc/desc

        $filter = $query['filter'];
        if (isset($query['user_id'])) {
            $comments = DB::table('comments')
                ->where('user_id', '=', $query['user_id'])
                ->where('text', 'LIKE', '%' .$filter.'%')
                ->orderBy($query['orderByField'], $query['orderByAsc'])
                ->get();
            return response(new Response($comments, HTTP_OK, ['description' => 'Comments get OK']))
                ->header('Content-Type', 'application/json');
        }
        if (isset($query['advert_id'])) {
            $comments = DB::table('comments')
                ->where('advert_id', '=', $query['advert_id'])
                ->where('text', 'LIKE', '%' .$filter.'%')
                ->orderBy($query['orderByField'], $query['orderByAsc'])
                ->get();
            return response(new Response($comments, HTTP_OK, ['description' => 'Comments get OK']))
                ->header('Content-Type', 'application/json');
        }
        else {
            $comments = Comment::where('text', 'LIKE', '%' . $query['filter'] . '%')
                ->orderBy($query['orderByField'], $query['orderByAsc'])
                ->get();

            return response(new Response($comments, HTTP_OK, ['description' => 'Comments get OK']))
                ->header('Content-Type', 'application/json');
        }
    }

    public function delComment(Request $request, $id){
        $requestParams = ['id' => $id];

        $validationRules = [
            'id' => 'required|integer|exists:comments,id'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationRules
        );

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error),
                HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }

        $comment = Comment::where('comments.id', '=', $id)
            ->delete();

        if ($comment) {
            return response(new Response([
                'description' => 'Comment delete OK'
            ], HTTP_OK, ['description' => 'Comment delete OK']));
        }

        return (new Response([
            'code' => HTTP_FORBIDDEN,
            'message' => 'Comment was not delete'
        ], HTTP_FORBIDDEN, ['description' => 'Access Forbidden']));

    }
}
