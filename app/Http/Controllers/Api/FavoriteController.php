<?php

namespace App\Http\Controllers\Api;

use App\Models\Favorite;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\DB;


class FavoriteController extends Controller
{
    //
    public function createFavorite(Request $request)
    {
        $requestParams = $request->only(
            'user_id',
            'advert_id'
        );

        $validationRules = [
            'user_id' => 'required|integer|exists:users,id',
            'advert_id' => 'required|integer|exists:adverts,id'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationRules
        );

        $requestParams['status'] = 1;

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error),
                HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }


        //need check file exist in image field

        $existFavorite = DB::table('favorites')
            ->where('user_id','=',$requestParams['user_id'])
            ->where('advert_id','=',$requestParams['advert_id'])
            ->get();
        if (count($existFavorite) > 0) {
            return (new Response([
                'code' => HTTP_FOUND,
                'message' => 'Favorite with these params already exists'
            ], HTTP_FOUND, ['description' => 'Favorite exists']));
        }

        $favorite = new Favorite($requestParams);
        $favorite->save();

        return response(new Response([
            'id' => $favorite->id,
            'user_id' => $requestParams['user_id'],
            'advert_id' => $requestParams['advert_id'],
        ], HTTP_OK, ['description' => 'Favorite create OK']));
    }

    public function getFavorite(Request $request){

        $query = $request->query();

        if (isset($query['user_id'])) {
            $favorites = DB::table('favorites')
                ->where('user_id', '=', $query['user_id'])
                ->get();
            return response(new Response($favorites, HTTP_OK, ['description' => 'Favorites get OK']))
                ->header('Content-Type', 'application/json');
        }
        else {
            $favorites = DB::table('favorites')->get();

            return response(new Response($favorites, HTTP_OK, ['description' => 'Favorites get OK']))
                ->header('Content-Type', 'application/json');
        }
    }

    public function delFavorite(Request $request, $id){
        $requestParams = ['id' => $id];

        $validationRules = [
            'id' => 'required|integer|exists:favorites,id'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationRules
        );

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error),
                HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }

        $advert = Favorite::where('favorites.id', '=', $id)
            ->delete();

        if ($advert) {
            return response(new Response([
                'description' => 'Favorite delete OK'
            ], HTTP_OK, ['description' => 'Favorite delete OK']));
        }

        return (new Response([
            'code' => HTTP_FORBIDDEN,
            'message' => 'Favorite was not delete'
        ], HTTP_FORBIDDEN, ['description' => 'Access Forbidden']));

    }
}
