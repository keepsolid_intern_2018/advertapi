<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\Metric;
use App\Models\Advert;
use Illuminate\Support\Facades\DB;

class AdvertController extends Controller
{
    //
    public function createAdvert(Request $request)
    {
        $requestParams = $request->only(
            'title',
            'description',
            'image',
            'price',
            'user_id',
            'category_id'
        );

        $validationRules = [
            'title' => 'required|min:3',
            'description' => 'required|min:8',
            'image' => 'required|min:5',
            'price' => 'required|numeric|min:1',
            'user_id' => 'required|integer|exists:users,id',
            'category_id' => 'required|integer|exists:subcategories,id'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationRules
        );

        $requestParams['status'] = 1;

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error),
                HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }


        //need check file exist in image field

        $existAdvert = DB::table('adverts')
            ->where('title','=',$requestParams['title'])
            ->where('description','=',$requestParams['description'])
            ->where('image','=',$requestParams['image'])
            ->where('price','=',$requestParams['price'])
            ->where('status','=',$requestParams['status'])
            ->where('user_id','=',$requestParams['user_id'])
            ->where('category_id','=',$requestParams['category_id'])
            ->get();
        if (count($existAdvert) > 0) {
            return (new Response([
                'code' => HTTP_FOUND,
                'message' => 'Advert with these params already exists'
            ], HTTP_FOUND, ['description' => 'Advert exists']));
        }

        $advert = new Advert($requestParams);
        $advert->save();

        return response(new Response([
            'id' => $advert->id,
            'title' => $requestParams['title'],
            'description' => $requestParams['description'],
            'image' => $requestParams['image'],
            'price' => $requestParams['price'],
            'status' => $requestParams['status'],
            'user_id' => $requestParams['user_id'],
            'category_id' => $requestParams['category_id'],
        ], HTTP_OK, ['description' => 'Advert create OK']));
    }

    public function getAdvert(Request $request){

        $query = $request->query();

        if (!isset($query['filter'])) $query['filter'] = '';
        if (!isset($query['orderByField'])) $query['orderByField'] = 'id';
        if (!isset($query['orderByAsc'])) $query['orderByAsc'] = 'asc';

        //check orderByField on exist field and orderByAsc on asc/desc

        $filter = $query['filter'];
        if (isset($query['category_id'])) {
            $adverts = DB::table('adverts')
                ->where('category_id', '=', $query['category_id'])
                ->where(function($query1) use ($filter) {
                    $query1->where('title', 'LIKE', '%' .$filter.'%');
                    $query1->orWhere('description', 'LIKE', '%' .$filter.'%');
                    $query1->orWhere('image', 'LIKE', '%' .$filter.'%');
                })
                ->orderBy($query['orderByField'], $query['orderByAsc'])
                ->get();
            return response(new Response($adverts, HTTP_OK, ['description' => 'Adverts get OK']))
                ->header('Content-Type', 'application/json');
        }
        else {
            $adverts = Advert::where('title', 'LIKE', '%' . $query['filter'] . '%')
                ->orWhere('description', 'LIKE', '%' . $query['filter'] . '%')
                ->orWhere('image', 'LIKE', '%' . $query['filter'] . '%')
                ->orderBy($query['orderByField'], $query['orderByAsc'])
                ->get();

            return response(new Response($adverts, HTTP_OK, ['description' => 'Adverts get OK']))
                ->header('Content-Type', 'application/json');
        }
    }
    public function getAdvertById(Request $request, $id){
        $requestParams = ['id' => $id];

        $validationRules = [
            'id' => 'required|integer|exists:adverts,id'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationRules
        );

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error),
                HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }

        $advert = DB::table('adverts')
            ->where('id','=',$id)
            ->get();

        //add metric on get this advert
        $metric = new Metric([
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'referer' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "",
            'user_agent' => $_SERVER['HTTP_USER_AGENT'],
            'advert_id' => $id
        ]);
        $metric->save();

        return response(new Response($advert, HTTP_OK, ['description' => 'Advert get by id OK']))
            ->header('Content-Type', 'application/json');

    }

    public function editAdvert(Request $request, $id){

        $requestParams = $request->only(
            'title',
            'description',
            'image',
            'price',
            'status',
            'user_id',
            'category_id'
        );
        $requestParams ['id'] = $id;
        $validationRules = [
            'id' => 'required|integer|exists:adverts,id',
            'title' => 'required|min:3',
            'description' => 'required|min:8',
            'image' => 'required|min:5',
            'price' => 'required|numeric|min:1',
            'status' => 'required|integer',
            'user_id' => 'required|exists:users,id',
            'category_id' => 'required|exists:subcategories,id'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationRules
        );

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error),
                HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }


        //need check file exist in image field

        $existAdvert = DB::table('adverts')
            ->where('title','=',$requestParams['title'])
            ->where('description','=',$requestParams['description'])
            ->where('image','=',$requestParams['image'])
            ->where('price','=',$requestParams['price'])
            ->where('status','=',$requestParams['status'])
            ->where('user_id','=',$requestParams['user_id'])
            ->where('category_id','=',$requestParams['category_id'])
            ->get();
        if (count($existAdvert) > 0) {
            return (new Response([
                'code' => HTTP_FOUND,
                'message' => 'Advert with these params already exists'
            ], HTTP_FOUND, ['description' => 'Advert exists']));
        }

        $advert = Advert::where('adverts.id', '=', $id)
            ->update([
                'title' => $requestParams['title'],
                'description' => $requestParams['description'],
                'image' => $requestParams['image'],
                'price' => $requestParams['price'],
                'status' => $requestParams['status'],
                'user_id' => $requestParams['user_id'],
                'category_id' => $requestParams['category_id']
            ]);

        if ($advert) {
            return response(new Response([
                'id' => $id,
                'title' => $requestParams['title'],
                'description' => $requestParams['description'],
                'image' => $requestParams['image'],
                'price' => $requestParams['price'],
                'status' => $requestParams['status'],
                'user_id' => $requestParams['user_id'],
                'category_id' => $requestParams['category_id'],
            ], HTTP_OK, ['description' => 'Advert edit OK']));
        }

        return (new Response([
            'code' => HTTP_FORBIDDEN,
            'message' => 'Advert was not edit'
        ], HTTP_FORBIDDEN, ['description' => 'Access Forbidden']));
    }

    public function delAdvert(Request $request, $id){
        $requestParams = ['id' => $id];

        $validationRules = [
            'id' => 'required|integer|exists:adverts,id'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationRules
        );

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error),
                HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }

        $advert = Advert::where('adverts.id', '=', $id)
            ->delete();

        if ($advert) {
            return response(new Response([
                'description' => 'Advert delete OK'
            ], HTTP_OK, ['description' => 'Advert delete OK']));
        }

        return (new Response([
            'code' => HTTP_FORBIDDEN,
            'message' => 'Advert was not delete'
        ], HTTP_FORBIDDEN, ['description' => 'Access Forbidden']));

    }
}
