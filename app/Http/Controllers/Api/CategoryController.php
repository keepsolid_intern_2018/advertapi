<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\MainCategory;
use App\Models\SubCategory;
use Illuminate\Support\Facades\DB;


class CategoryController extends Controller
{
    //MainCategory
    public function createMainCategory(Request $request)
    {
        $requestParams = $request->only(
            'name'
        );

        $validationRules = [
            'name' => 'required|min:3'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationRules
        );

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error),
                HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }

        $existMainCategory = DB::table('maincategories')
            ->where('name', '=', $requestParams['name'])
            ->get();
        if (count($existMainCategory) > 0) {
            return (new Response([
                'code' => HTTP_FOUND,
                'message' => 'Maincategory with these name already exists'
            ], HTTP_FOUND, ['description' => 'Maincategory exists']));
        }

        $mainCategory = new MainCategory($requestParams);
        $mainCategory->save();

        return response(new Response([
            'id' => $mainCategory->id,
            'name' => $requestParams['name']
        ], HTTP_OK, ['description' => 'Maincategory create OK']));

    }

    public function getMainCategory(Request $request){

        $query = $request->query();

        if (!isset($query['filter'])) $query['filter'] = '';

        $mainCategories = MainCategory::where('name', 'LIKE', '%' . $query['filter'] . '%')->get();

        return response(new Response($mainCategories, HTTP_OK, ['description' => 'MainCategories get OK']))
            ->header('Content-Type', 'application/json');

    }

    public function getMainCategoryById(Request $request, $id){
        $requestParams = ['id' => $id];

        $validationRules = [
            'id' => 'required|integer|exists:maincategories,id'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationRules
        );

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error),
                HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }

        $mainCategory = DB::table('maincategories')
            ->where('id','=',$id)
            ->get();

        return response(new Response($mainCategory, HTTP_OK, ['description' => 'MainCategory get by id OK']))
            ->header('Content-Type', 'application/json');

    }

    public function editMainCategory(Request $request, $id){

        $requestParams = $request->only(
            'name'
        );
        $requestParams ['id'] = $id;
        $validationRules = [
            'id' => 'required|integer|exists:maincategories,id',
            'name' => 'required|min:3'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationRules
        );

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error),
                HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }

        $existMainCategory = DB::table('maincategories')
            ->where('name','=',$requestParams['name'])
            ->get();
        if (count($existMainCategory) > 0) {
            return (new Response([
                'code' => HTTP_FOUND,
                'message' => 'MainCategory with these params already exists'
            ], HTTP_FOUND, ['description' => 'MainCategory exists']));
        }

        $mainCategory = MainCategory::where('maincategories.id', '=', $id)
            ->update([
                'name' => $requestParams['name']
            ]);

        if ($mainCategory) {
            return response(new Response([
                'id' => $id,
                'name' => $requestParams['name'],
            ], HTTP_OK, ['description' => 'MainCategory edit OK']));
        }

        return (new Response([
            'code' => HTTP_FORBIDDEN,
            'message' => 'MainCategory was not edit'
        ], HTTP_FORBIDDEN, ['description' => 'Access Forbidden']));
    }

    public function delMainCategory(Request $request, $id){
        $requestParams = ['id' => $id];

        $validationRules = [
            'id' => 'required|integer|exists:maincategories,id'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationRules
        );

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error),
                HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }

        $mainCategory = MainCategory::where('maincategories.id', '=', $id)
            ->delete();

        if ($mainCategory) {
            return response(new Response([
                'description' => 'MainCategory delete OK'
            ], HTTP_OK, ['description' => 'MainCategory delete OK']));
        }

        return (new Response([
            'code' => HTTP_FORBIDDEN,
            'message' => 'MainCategory was not delete'
        ], HTTP_FORBIDDEN, ['description' => 'Access Forbidden']));

    }


    //SubCategory
    public function createSubCategory(Request $request)
    {
        $requestParams = $request->only(
            'name',
            'maincategory_id'
        );

        $validationRules = [
            'name' => 'required|min:3',
            'maincategory_id' => 'required|integer|exists:maincategories,id'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationRules
        );

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error),
                HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }

        $existSubCategory = DB::table('subcategories')
            ->where('name','=',$requestParams['name'])
            ->where('maincategory_id','=',$requestParams['maincategory_id'])
            ->get();
        if (count($existSubCategory) > 0) {
            return (new Response([
                'code' => HTTP_FOUND,
                'message' => 'Subcategory with these params already exists'
            ], HTTP_FOUND, ['description' => 'Subcategory exists']));
        }

        $advert = new SubCategory($requestParams);
        $advert->save();

        return response(new Response([
            'id' => $advert->id,
            'name' => $requestParams['name'],
            'maincategory_id' => $requestParams['maincategory_id']
        ], HTTP_OK, ['description' => 'Subcategory create OK']));
    }

    public function getSubCategory(Request $request){

        $query = $request->query();

        if (!isset($query['filter'])) $query['filter'] = '';

        $filter = $query['filter'];
        if (isset($query['maincategory_id'])) {
            $subCategories = DB::table('subcategories')
                ->where('maincategory_id', '=', $query['maincategory_id'])
                ->where('name', 'LIKE', '%' .$filter.'%')
                ->get();
            return response(new Response($subCategories, HTTP_OK, ['description' => 'Adverts get OK']))
                ->header('Content-Type', 'application/json');
        }
        else {
            $subCategories = SubCategory::where('name', 'LIKE', '%' . $query['filter'] . '%')->get();

            return response(new Response($subCategories, HTTP_OK, ['description' => 'Adverts get OK']))
                ->header('Content-Type', 'application/json');
        }
    }

    public function getSubCategoryById(Request $request, $id){
        $requestParams = ['id' => $id];

        $validationRules = [
            'id' => 'required|integer|exists:subcategories,id'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationRules
        );

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error),
                HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }

        $subCategory = DB::table('subcategories')
            ->where('id','=',$id)
            ->get();

        return response(new Response($subCategory, HTTP_OK, ['description' => 'SubCategory get by id OK']))
            ->header('Content-Type', 'application/json');

    }

    public function editSubCategory(Request $request, $id){

        $requestParams = $request->only(
            'name',
            'maincategory_id'
        );
        $requestParams ['id'] = $id;
        $validationRules = [
            'id' => 'required|integer|exists:subcategories,id',
            'name' => 'required|min:3',
            'maincategory_id' => 'required|exists:maincategories,id'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationRules
        );

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error),
                HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }


        $existSubCategory = DB::table('subcategories')
            ->where('name','=',$requestParams['name'])
            ->where('maincategory_id','=',$requestParams['maincategory_id'])
            ->get();
        if (count($existSubCategory) > 0) {
            return (new Response([
                'code' => HTTP_FOUND,
                'message' => 'SubCategory with these params already exists'
            ], HTTP_FOUND, ['description' => 'SubCategory exists']));
        }

        $subCategory = SubCategory::where('subcategories.id', '=', $id)
            ->update([
                'name' => $requestParams['name'],
                'maincategory_id' => $requestParams['maincategory_id']
            ]);

        if ($subCategory) {
            return response(new Response([
                'id' => $id,
                'name' => $requestParams['name'],
                'maincategory_id' => $requestParams['maincategory_id'],
            ], HTTP_OK, ['description' => 'SubCategory edit OK']));
        }

        return (new Response([
            'code' => HTTP_FORBIDDEN,
            'message' => 'SubCategory was not edit'
        ], HTTP_FORBIDDEN, ['description' => 'Access Forbidden']));
    }

    public function delSubCategory(Request $request, $id){
        $requestParams = ['id' => $id];

        $validationRules = [
            'id' => 'required|integer|exists:subcategories,id'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationRules
        );

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error),
                HTTP_BAD_REQUEST, ['description' => 'Invalid params passed'])
            );
        }

        $subCategory = SubCategory::where('subcategories.id', '=', $id)
            ->delete();

        if ($subCategory) {
            return response(new Response([
                'description' => 'SubCategory delete OK'
            ], HTTP_OK, ['description' => 'SubCategory delete OK']));
        }

        return (new Response([
            'code' => HTTP_FORBIDDEN,
            'message' => 'SubCategory was not delete'
        ], HTTP_FORBIDDEN, ['description' => 'Access Forbidden']));
    }
}
