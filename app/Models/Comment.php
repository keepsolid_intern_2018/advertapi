<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Comment extends Model
{
    //
    use Notifiable;

    protected $table = 'comments';

    protected $fillable = ['text', 'user_id', 'advert_id'];
}
