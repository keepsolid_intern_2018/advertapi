<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Metric extends Model
{
    //
    use Notifiable;

    protected $table = 'metrics';

    protected $fillable = ['ip_address', 'referer', 'user_agent', 'advert_id'];

}
