<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class MainCategory extends Model
{
    //
    use Notifiable;

    protected $table = 'maincategories';

    protected $fillable = ['name'];

    public $timestamps = false;
}
