<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Favorite extends Model
{
    //
    use Notifiable;

    protected $table = 'favorites';

    protected $fillable = ['user_id', 'advert_id'];
}
