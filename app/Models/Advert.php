<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Advert extends Model
{
    //
    use Notifiable;

    protected $table = 'adverts';

    protected $fillable = ['title', 'description', 'image', 'price', 'status', 'user_id', 'category_id'];
}
