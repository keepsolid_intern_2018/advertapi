<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class SubCategory extends Model
{
    //
    use Notifiable;

    protected $table = 'subcategories';

    protected $fillable = ['name', 'maincategory_id'];

    public $timestamps = false;
}
